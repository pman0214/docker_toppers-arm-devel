# TOPPERS on ARM Development

## Overview

* Docker image for development of [TOPPERS](https://www.toppers.jp) running on ARM devices.
	* My target is [GR-Peach](https://www.renesas.com/products/gadget-renesas/boards/gr-peach.html).
* Installs:
	* gcc-arm-none-eabi
	* for configurator building:
		* build-essential
		* libboost-all-dev
* Also provides a sample `docker-compose.yml` file.

## Copyright, License

Copyright (c) 2020, Shigemi ISHIDA

* `Dockerfile` is released under the BSD 3-clause license.
  See `LICENSE_BSD-3Clause`.
* `docker-compose.yml` is released under the MIT license.
  See `LICENSE_MIT`.
